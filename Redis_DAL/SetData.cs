﻿using Redis_Factory;
using ServiceStack.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Redis_DAL
{
    public class SetData
    {

		public bool AddKey<T>( string key,T value)
        {
            if (string.IsNullOrEmpty(key))
                return false;
            if (value == null)
                return false;

            try
            {
                if (Singleton.Instance.RedisClient== null)
                    return false;
                Singleton.Instance.RedisClient.Set(key, value);
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return false;
        }

        public bool AddList<T>(string key,T value)
        {
            if (string.IsNullOrEmpty(key))
                return false;
            if (value == null)
                return false;

            try
            {
                if(Singleton.Instance.RedisClient != null )
                {
                    //Singleton.Instance.RedisClient.AddItemToList(key, Newtonsoft.Json.JsonConvert.SerializeObject(value));
                    string json = Newtonsoft.Json.JsonConvert.SerializeObject(value);
                    byte[] bt = Encoding.UTF8.GetBytes(json);
                    Singleton.Instance.RedisClient.RPush(key, bt);
                    return true;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return false;
        }

        /// <summary>
        /// 修改数据
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="index"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public bool UploadList<T>(string key,int index,T value)
        {
            if (string.IsNullOrEmpty(key))
                return false;
            if (index < 0)
                return false;
            if (value == null)
                return false;

            try
            {
                if(Singleton.Instance.RedisClient != null)
                {
                    string json = Newtonsoft.Json.JsonConvert.SerializeObject(value);
                    byte[] bt = Encoding.UTF8.GetBytes(json);
                    Singleton.Instance.RedisClient.LSet(key, index, bt);
                    return true;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return false;
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public bool DeleteList<T>(string key,T value,int count = 0)
        {
            if (string.IsNullOrEmpty(key))
                return false;
            if (value == null)
                return false;

            try
            {
                if(Singleton.Instance.RedisClient != null)
                {

                    string json = Newtonsoft.Json.JsonConvert.SerializeObject(value);
                    byte[] bt = Encoding.UTF8.GetBytes(json);
                    Singleton.Instance.RedisClient.LRem(key, count, bt);
                    return true;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return false;
        }
    }
}
