﻿using Redis_Factory;
using ServiceStack.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Redis_DAL
{
    public class GetData
    {
        public T GetKey<T>( string key)
        {
            if (string.IsNullOrEmpty(key))
                return default(T);
            T obj = default(T);
            try
            {
                if (Singleton.Instance.RedisClient != null)
                {
                    obj = Singleton.Instance.RedisClient.Get<T>(key);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return obj;
        }


        public List<T> GetList<T>(string key)
        {
            List<T> list = new List<T>();
            if (string.IsNullOrEmpty(key))
                return list;

            try
            {
                if(Singleton.Instance.RedisClient != null)
                {
                    byte[][] bt = Singleton.Instance.RedisClient.LRange(key, 0, - 1);
                    string str = string.Empty;
                    for(int i = 0;i<bt.Length;i++)
                    {
                        str = Encoding.UTF8.GetString(bt[i]);
                        list.Add(Newtonsoft.Json.JsonConvert.DeserializeObject<T>(str));
                    }
                    //list = Singleton.Instance.RedisClient.Get<List<T>>(key);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return list;
        }
            
    }
}
