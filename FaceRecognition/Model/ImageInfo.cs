﻿using OpenCvSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaceRecognition.Model
{
    public class ImageInfo
    {
        public int ImageID { get; set; }
        public string Name { get; set; }
        public string Gender { get; set; }

        /// <summary>
        /// 头像数量
        /// </summary>
        public int ImageGroupID { get; set; }  
        public Mat Image { get; set; }

        /// <summary>
        /// 头像
        /// </summary>
        public string HeadImage { get; set; }
    }
}
