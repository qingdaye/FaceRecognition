﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FaceRecognition
{
    static class Program
    {
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            runServers();
            Application.Run(new FrmUserInfo());
        }

        private static void runServers()
        {
            System.Threading.Thread th = new System.Threading.Thread(() =>
            {
                System.Diagnostics.Process[] processList = System.Diagnostics.Process.GetProcesses();
                foreach (System.Diagnostics.Process item in processList)
                {
                    if (item.ProcessName.ToLower().Equals("redis-server"))
                    {
                        return;
                    }
                }

                string redisPath = Application.StartupPath + "\\Redis";
                if (!System.IO.Directory.Exists(redisPath))
                {
                    throw new Exception("数据库启动失败！");
                }
                System.Diagnostics.Process p = new System.Diagnostics.Process();

                p.StartInfo.FileName = "cmd.exe";
                p.StartInfo.UseShellExecute = false;
                p.StartInfo.RedirectStandardInput = true;
                p.StartInfo.RedirectStandardOutput = true;
                p.StartInfo.RedirectStandardError = true;
                p.StartInfo.CreateNoWindow = true;
                p.Start();

                p.StandardInput.WriteLine(redisPath + "\\redis-server.exe " + redisPath + "\\redis.windows.conf" + "&exit");

                p.StandardInput.AutoFlush = true;
                string s = p.StandardOutput.ReadToEnd();
                Console.WriteLine(s);
                p.WaitForExit();//等待程序执行完退出进程
            });
            th.Start();
        }
    }
}
