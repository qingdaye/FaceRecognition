﻿using FaceRecognition.Model;
using OpenCvSharp.Face;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaceRecognition
{
    /// <summary>
    /// 人脸识别训练
    /// </summary>
    public class FaceTrain
    {
        private List<ImageInfo> images = new List<ImageInfo>();

        /// <summary>
        /// 数据准备以及人脸识别器的训练
        /// </summary>
        private void GetFaceRecognizer()
        {
            if (images == null || images.Count < 1)
                return;
            FisherFaceRecognizer faceRecognizer = FisherFaceRecognizer.Create();
            faceRecognizer.Train(images.Select(x => x.Image), images.Select(x => x.ImageGroupID));
        }

        private void GetImageInfos()
        {

        }

    }
   
}
