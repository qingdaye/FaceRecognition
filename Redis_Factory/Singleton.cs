﻿
using ServiceStack.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Redis_Factory
{
    public class Singleton
    {
        private static object lockObj = new object();
        private static Singleton mSetData;
        public static Singleton Instance
        {
            get
            {
                if (mSetData == null)
                {
                    lock (lockObj)
                    {
                        if (mSetData == null)
                            mSetData = new Singleton();
                    }
                }
                return mSetData;
            }
        }



        private RedisClient mRedisClient;
        public RedisClient RedisClient
        {
            get
            {
                if (this.mRedisClient == null)
                    this.mRedisClient = GetRedisClient.Get();
                return this.mRedisClient;
            }
        }

     
        
    }
}
